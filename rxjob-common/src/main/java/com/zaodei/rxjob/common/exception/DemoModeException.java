package com.zaodei.rxjob.common.exception;

/**
 * 
 *@Title:演示模式异常
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月10日
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
