package com.zaodei.rxjob.common.enums;


/**
 *@Title:用户状态
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月9日
 */
public enum UserStatus {
    OK("0", "正常"), DISABLE("1", "停用"), DELETED("2", "删除");

    private final String code;
    private final String info;

    UserStatus(String code, String info)
    {
        this.code = code;
        this.info = info;
    }

    public String getCode()
    {
        return code;
    }

    public String getInfo()
    {
        return info;
    }
}
