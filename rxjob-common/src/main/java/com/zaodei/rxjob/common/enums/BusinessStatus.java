package com.zaodei.rxjob.common.enums;


/**
 *@Title:操作状态
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月9日
 */
public enum BusinessStatus {
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
