package com.zaodei.rxjob;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @Title:启动程序
 * @Description:
 * @Author:wuche@laozo.com
 * @time:2019年1月9日
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
@MapperScan({ "com.zaodei.rxjob.*.mapper", "com.zaodei.rxjob.*.dao" })
public class RxjobApplication {
	public static void main(String[] args) {
		// System.setProperty("spring.devtools.restart.enabled", "false");
		SpringApplication.run(RxjobApplication.class, args);
		System.out.println("(♥◠‿◠)ﾉﾞ  Rxjob启动成功   ლ(´ڡ`ლ)ﾞ  \n"
				+ " ''-'   `'-'    `-..-'              ");
	}
}
