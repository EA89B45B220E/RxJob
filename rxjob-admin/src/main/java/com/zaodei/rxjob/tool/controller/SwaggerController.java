package com.zaodei.rxjob.tool.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.zaodei.rxjob.core.web.base.BaseController;

/**
 * 
 *@Title:swagger 接口
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月10日
 */
@Controller
@RequestMapping("/tool/swagger")
public class SwaggerController extends BaseController
{
    @RequiresPermissions("tool:swagger:view")
    @GetMapping()
    public String index()
    {
        return redirect("/swagger-ui.html");
    }
}
