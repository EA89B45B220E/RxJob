package com.zaodei.rxjob.core.web.exception.user;

/**
 * 
 *@Title:角色锁定异常类
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月10日
 */
public class RoleBlockedException extends UserException
{
    private static final long serialVersionUID = 1L;

    public RoleBlockedException(String reason)
    {
        super("role.blocked", new Object[] { reason });
    }
}
