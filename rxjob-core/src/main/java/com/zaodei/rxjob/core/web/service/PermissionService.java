package com.zaodei.rxjob.core.web.service;

import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Service;


/**
 *@Title: 首创 js调用 thymeleaf 实现按钮权限可见性
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月10日
 */
@Service("permission")
public class PermissionService {
    public String hasPermi(String permission)
    {
        return isPermittedOperator(permission) ? "" : "hidden";
    }

    private boolean isPermittedOperator(String permission)
    {
        return SecurityUtils.getSubject().isPermitted(permission);
    }
}
