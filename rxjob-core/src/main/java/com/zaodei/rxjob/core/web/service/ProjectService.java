package com.zaodei.rxjob.core.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zaodei.rxjob.core.util.ShiroUtils;
import com.zaodei.rxjob.system.domain.Project;
import com.zaodei.rxjob.system.service.IProjectService;

/**
 * 
 * @author wuche@laozo.com
 *
 */
@Service("project")
public class ProjectService {
	@Autowired
	private IProjectService projectService;

	public List<Project> getProject() {
		Project project = new Project();
		project.setStatus("0");
		project.setUserId(ShiroUtils.getUserId());
		return this.projectService.selectProjects(project);
	}
}
