package com.zaodei.rxjob.job.dao;

import com.zaodei.rxjob.job.admin.core.model.JobGroupProject;


/**
 * 项目 - 执行器数据层
 * 
 * @author wuchanghao
 * @date 2019-02-21
 */
public interface ProjectJobGroupdao{
	
	
	/**
	 * 新增项目任务
	 * @param id
	 * @return
	 */
	public int insertProjectJobGroup(JobGroupProject jobGroupProject);
	
	
	/**
	 * 删除项目执行器
	 * @param id
	 * @return
	 */
	public int deleteProjectJobGroup(JobGroupProject jobGroupProject);
	
	/**
	 * 查询项目执行器
	 * @param id
	 * @return
	 */
	public  JobGroupProject selectProjectJobGroup(int id);
	/**
	 * 查询项目执行器权限
	 * @param id
	 * @return
	 */
	public  JobGroupProject selectProjectJobGroupUser(JobGroupProject jobGroupProject);
	
	//删除项目执行器
	public int removeMoreProjectJobGroup(Integer[] ids);
}