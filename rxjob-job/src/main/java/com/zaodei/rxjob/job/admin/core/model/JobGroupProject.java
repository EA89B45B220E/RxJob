package com.zaodei.rxjob.job.admin.core.model;

public class JobGroupProject {
	// 执行器ID
	private int jobGroup;
	// 项目ID
	private Long projectId;

	// 用户ID
	private Long userId;

	// 角色ID
	private Long roleId;

	public synchronized int getJobGroup() {
		return jobGroup;
	}

	public synchronized void setJobGroup(int jobGroup) {
		this.jobGroup = jobGroup;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

}
