-- ----------------------------
-- 1、项目表  增加跟项目挂钩的业务
-- ----------------------------
drop table if exists sys_project;
 create table sys_project (
   project_id           int(11)         auto_increment    comment '项目id',
   project_name         varchar(30)     default ''        comment '项目名称',
   status 			char(1) 		default '0' 			   comment '项目状态（0正常 1停用）',
   del_flag			char(1) 		default '0' 			   comment '删除标志（0代表存在 2代表删除）',
   remark 			varchar(500) 	default '' 				   comment '备注',
   create_by         varchar(64)     default ''                 comment '创建者',
   create_time 	    datetime                                   comment '创建时间',
   update_by         varchar(64)     default ''                 comment '更新者',
   update_time       datetime                                   comment '更新时间',
   primary key (project_id)
) engine=innodb auto_increment=50 default charset=utf8 comment = '项目表';

-- ----------------------------
-- 2、项目和用户关联表  跟用户做关联
-- ----------------------------
drop table if exists sys_project_user;
create table sys_project_user (
  project_id 	int(11) not null comment '项目ID',
  user_id 	int(11) not null comment '用户ID',
  role_id  	int(11) not null comment '角色ID',
  primary key(project_id, user_id)
) engine=innodb default charset=utf8 comment = '项目和用户关联表';


-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, url,menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('项目管理', '1', '10', '/system/project', 'C', '0', 'system:project:view', '#', 'admin', '2019-03-01', 'dlbe', '2019-03-01', '项目管理菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu  (menu_name, parent_id, order_num, url,menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('项目查询', @parentId, '1',  '#',  'F', '0', 'system:project:list',         '#', 'admin', '2019-03-01', 'dlbe', '2019-03-01', '');

insert into sys_menu  (menu_name, parent_id, order_num, url,menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('项目新增', @parentId, '2',  '#',  'F', '0', 'system:project:add',          '#', 'admin', '2019-03-01', 'dlbe', '2019-03-01', '');

insert into sys_menu  (menu_name, parent_id, order_num, url,menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('项目修改', @parentId, '3',  '#',  'F', '0', 'system:project:edit',         '#', 'admin', '2019-03-01', 'dlbe', '2019-03-01', '');

insert into sys_menu  (menu_name, parent_id, order_num, url,menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('项目删除', @parentId, '4',  '#',  'F', '0', 'system:project:remove',       '#', 'admin', '2019-03-01', 'dlbe', '2019-03-01', '');



-- ----------------------------
-- 3、项目和执行器关联表  
-- ----------------------------
drop table if exists sys_project_job_group;
create table sys_project_job_group (
  project_id 	int(11) not null comment '项目ID',
  job_group 	int(11) not null comment '执行器主键ID',
  primary key(project_id, job_group)
) engine=innodb default charset=utf8 comment = '项目和执行器关联表';


