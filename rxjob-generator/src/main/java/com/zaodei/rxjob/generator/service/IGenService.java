package com.zaodei.rxjob.generator.service;

import java.util.List;
import com.zaodei.rxjob.generator.domain.TableInfo;

/**
 * 
 *@Title:代码生成 服务层
 *@Description:
 *@Author:wuche@laozo.com
 *@time:2019年1月10日
 */
public interface IGenService
{
    /**
     * 查询ry数据库表信息
     * 
     * @param tableInfo 表信息
     * @return 数据库表列表
     */
    public List<TableInfo> selectTableList(TableInfo tableInfo);

    /**
     * 生成代码
     * 
     * @param tableName 表名称
     * @return 数据
     */
    public byte[] generatorCode(String tableName);

    /**
     * 批量生成代码
     * 
     * @param tableNames 表数组
     * @return 数据
     */
    public byte[] generatorCode(String[] tableNames);
}
